import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class CustomerService {

  customerTypeList:any[];
  constructor(private http: HttpClient) {
    this.customerTypeList=[
      {
        id:1,
        name:'Bronze'
      },
      {
        id:2,
        name:'Silver'
      },
      {
        id:3,
        name:'Golden'
      }
    ]
  }

  add(entity) {
    return this.http.post("url", entity);
  }
  update(entity) {
    return this.http.put("url", entity);
  }
  getList() {
    return this.http.get("url");
  }
  getById(id) {
    return this.http.get("url" + id);
  }
  delete(id) {
    return this.http.delete("url" + id);
  }
}
