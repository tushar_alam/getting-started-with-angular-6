import { Component, OnInit, Input } from "@angular/core";
import { CustomerService } from "../customer.service";

@Component({
  selector: "app-customer-type",
  templateUrl: "./customer-type.component.html",
  styleUrls: ["./customer-type.component.css"]
})
export class CustomerTypeComponent implements OnInit {
  showList: boolean = false;

  @Input("message")
  message: string = "hello world";

  constructor(private customertypeService: CustomerService) {}

  ngOnInit() {}

  toggleList() {
    this.showList = !this.showList;
  }
}
