import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CustomerComponent } from "./customer/customer.component";
import { CustomerTypeComponent } from "./customer-type/customer-type.component";
import { combineLatest } from "rxjs";
import { NotFoundComponent } from "./not-found/not-found.component";
import { SettingsComponent } from "./settings/settings.component";
import { PaymentComponent } from "./payment/payment.component";

const routes: Routes = [
  { path: "", redirectTo: "/customer", pathMatch: "full" },
  { 
    path: "customer", 
    component: CustomerComponent,
    children:[
      { path: "", redirectTo: "settings", pathMatch: "full" },
      { path: "settings", component: SettingsComponent },
      { path: "payment", component: PaymentComponent },
    ]
   },
  { path: "customerType", component: CustomerTypeComponent },
  { path: "**",component:NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
